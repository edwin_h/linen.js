# linen.js

Create an "linen" style texture with HTML5 canvas for use in web pages.

## Usage

Include the **linen.js** file in your HTML: 

    <script src="linen.js"></script>

When you're ready to create a linen texture, call `linen()`:

    document.body.style.backgroundImage = linen();

*Optionally,* you can specify some parameters:

    document.body.style.backgroundImage = linen({
    	size: 256,
    	density: 90,
    	lightness: 24
    });

### List of parameters: 

`size`: The width and height (px) of the texture. Higher is better, but might take longer to generate. *Default is 256*

`threadLength`: Changes the length (px) of each thread. *Default is 24*

`threadWidth`: Changes the width (px) of each thread. *Default is 1*

`density`: Changes the density of the threads. *Between 0 and 100, default is 80*

`lightness`: Changes the overall lightness of the linen. *Between 0 and 100, default is 16*

`lightnessVariance`: Changes how varied the lightness of the threads are (based on the `lightness` value). *Between 0 and 100, default is 1*

`tintColor`: Changes the overall color of the linen. *Use HSL, RGB, or a hex value, default is `hsl(200,100%,50%)` (a blue color)*

`tintAmount`: Changes the opacity of the tint. *Between 0 and 100, default is 5*

`returnType`: Changes the return type. Use `dataURL` (returns a CSS data URL) or `image` (returns an image object). 