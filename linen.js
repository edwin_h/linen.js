var linen = function(params){
	if(typeof(params) == "undefined"){
		var params = {};
	}
	var size = params.size || 256;
	var threadLength = params.threadLength || 24;
	var threadWidth = (params.threadWidth || 1) + "px";
	var density = params.density || 80;
	var lightness = params.lightness || 16;
	var lightnessVariance = params.lightnessVariance || 1;
	var tintColor = params.tintColor || "hsl(200,100%,50%)";
	var tintAmount = params.tintAmount || "5";
	var returnType = params.returnType || "dataURL"

	var random = function(min,max){
		return ~~(Math.random()*max)+min
	}

	var canvas = document.createElement("canvas");
	var ctx = canvas.getContext("2d");

	canvas.width = canvas.height = size;

	ctx.fillStyle = "hsl(0,0%,"+lightness+"%)";
	ctx.fillRect(0, 0, size, size);

	var lMin = lightness - lightnessVariance;
	var lMax = lightness + lightnessVariance;
	
	var thick = 100 - density;
	var thickLucky = Math.floor(thick / 2);

	var threadLengthHalf = threadLength / 2;


	for(var row = 0; row < size; row++){
		for(var col = 0; col < size; col++){
			var lit;
			if(lMin != lMax){
				lit = random(lMin, lMax);
			}else{
				lit = lMin || lMax;
			}
			ctx.strokeStyle = "hsl(0,0%,"+lit+"%)";
			ctx.lineWidth = threadWidth;
			ctx.beginPath();

			if(random(0,thick) == thickLucky){
				var startCol = col - (threadLengthHalf);
				var endCol = col + (threadLengthHalf);
				var startRow = row - (threadLengthHalf);
				var endRow = row + (threadLengthHalf);
				if(random(1,2) == 1){
					// Draw horizontal
					ctx.moveTo(startCol, row);
					ctx.lineTo(endCol, row);
				}else{
					// Draw vertical
					ctx.moveTo(col, startRow);
					ctx.lineTo(col, endRow);
				}
			}			
			ctx.stroke();
			ctx.closePath();
		}
	}

	ctx.globalAlpha = tintAmount * 0.01;
	ctx.fillStyle = tintColor;
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	if(returnType === "dataURL"){
		return "url("+canvas.toDataURL("image/png")+")";
	}else if(returnType === "image"){
		var img = new Image();
		img.src = canvas.toDataURL("image/png");
		return img;
	}else{
		console.log("linen.js: returnType not recognized. Use either \"dataURL\" or \"image\".");
	}
	

}